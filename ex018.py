#coding:utf-8
#求s=a+aa+aaa+aaaa+aa...a的值，其中a是一个数字。例如2+22+222+2222+22222(此时共有5个数相加)，几个数相加由键盘控制

a = int(raw_input("Which basic number do you want to compute? >: "))
n = int(raw_input("How many combined numbers do you want to compute? >: "))

mylist = map(lambda s: str(s), range(1, n + 1))
#map是list的特殊操作，相当于for item in list这样的循环
#lambda是一个特殊函数，用于快速定义单行函数
mycount = int(''.join(mylist))

print a * mycount