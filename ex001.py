#coding:utf-8
from random import shuffle


def give3():
	list4 = ['1', '2', '3', '4']
	shuffle(list4) #random里面的shuffle方法是不返回值的，直接对对象进行操作
	list4.pop(3) #pop方法返回的是被pop出去的元素，但是同时也会直接对对象进行操作
	list3 = ''.join(list4)
	num3 = int(list3)
	return num3

num_list = [] #建立空list

for i in range(100): #循环100次应该够了
	#num_list[i] = give3(); #这个语句是错误的，给list增加元素不能直接用=，而应该用.append方法
	num_list.append(give3())
	
num_list = list(set(num_list)) #用set方法去除重复元素
len_list = len(num_list)

print "There are %d numbers that can be generated." % len_list
print "These numbers are:", num_list