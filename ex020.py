#coding:utf-8
#一球从100米高度自由落下，每次落地后反跳回原高度的一半；再落下，求它在第10次落地时，共经过多少米？第10次反弹多高？

n = 10
dis = 100 #第一次落地时旅行距离是100米，第二次落地增加100米（弹跳高度*2）

if n > 1:
	for i in range(n - 1):
		h = 100.00 / 2 ** i #注意：控制精度要在计算前
		#这是第i+1次落地后到第i+2次落地之间的旅行距离，比如i=0时，是第1、2次落地之间的距离
		dis += h
	print "The ball go up to %.1f meters." % (h / 4)
	#题目要得出第n次落地后跳起的高度，但前面只循环到i=n-2（第n-1次落地与第n次落地之间的距离）
	#因此这里除了要把前面的距离除以2之外，还要再继续迭代一次，相当于再除以2
else:
	print "The ball goes up to 50 meters."
print "The ball travels %.1f meters in total." % dis