#coding:utf-8
#一个整数，它加上100后是一个完全平方数，再加上168又是一个完全平方数，请问该数是多少？
from math import sqrt

for x in range(-100, 84 * 84 - 167):
#假设x+100=a1^2, x+268=a2^2,则(a2+a1)*(a2-a1)=168，则要求a2+a1和a2-a1奇偶性相同
#则可发现a2-a1最小为2，对应的a2+a1最大为84，则可得出x的最大范围
#另外，x肯定是大于-100的
	if sqrt(x + 100) == int(sqrt(x + 100)) and sqrt(x + 268) == int(sqrt(x + 268)):
	#注意，使用isinstance(sqrt(), int)来判断是错误的，因为这个函数认为3.0 != 3
		print x

print '-' * 20
print 'Another method:'

for a1 in range(0, 42):
#进一步分析可以发现a1最大为41
	for a2 in range(0, 84):
		if a2**2 - a1**2 == 168:
			print a1**2 - 100