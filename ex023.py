#coding:utf-8

n = int(raw_input("How many rows(should be odd number) do you want to display? >: "))

list1 = range(1, n + 1, 2)
list2 = range(1, n + 1, 2)
#list2 = list1是错的, 因为在对list1或者list2进行任何操作时会同时修改另一个, 也就是说对列表来说，"list2 = list1"的含义是"无论后续进行任何修改，都让list2 == list1"
#list3 = list1 + list2.reverse().pop(0)是错的，因为list2.reverse()不返回任何值，因此.pop(0)是对一个空对象进行操作
list2.reverse()
list2.pop(0)
list3 = list1 + list2

for item in list3:
	print " " * ((n - item) / 2), "*" * item