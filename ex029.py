#coding:utf-8
#给一个不多于5位的正整数，要求：一、求它是几位数，二、逆序打印出各位数字

s = raw_input("Give me a number containing no more than 5 numbers>: ")
r = range(len(s)) #字符串没有reverse方法，所以要另建一个经过reverse处理的index
r.reverse()

print '-' * 20
print "There are %d numbers in it." % len(s)
print '-' * 20
print "These numbers are(from last to first):"
for i in r:
	print s[i]