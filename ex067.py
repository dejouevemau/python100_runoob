#coding:utf-8
#输入数组，最大的与第一个元素交换，最小的与最后一个元素交换，输出数组

str0 = raw_input("Give me a list like [1,3,2](w/o brackets:> ")
list0 = [int(i) for i in str0.split(',')] #注意列表推導式的用法
list1 = [0] * len(list0)

mymax = max(list0)
mymin = min(list0)

for i in range(len(list0)):
    if list0[i] == mymax:
        list1[i] = list0[0]
    elif list0[i] == mymin:
        list1[i] = list0[-1]
    else:
        list1[i] = list0[i]

list1[0] = mymax
list1[-1] = mymin

print list1