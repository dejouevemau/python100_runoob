#coding:utf-8
#有一分数序列：2/1，3/2，5/3，8/5，13/8，21/13...求出这个数列的前20项之和

list1 = [1.0, 2.0] #需要一位小数,好进行浮点数运算
list2 = [2.0 / 1.0]

for i in range(19):
	list1.append(list1[-1] + list1[-2])
	list2.append(list1[-1] / list1[-2])
	
print sum(list2)