#coding:utf-8
#有n个人围成一圈，顺序排号。从第一个人开始报数（从1到3报数），凡报到3的人退出圈子，问最后留下的是原来第几号的那位

n = int(raw_input("How many people are there?:> "))

list0 = range(1, n + 1) #還沒有開始報數的時候
countend = 0

while True:
    for i in range(n):
        if list0[i] > 0:
            countend += 1 #接着上一個循環來報數
        if countend % 3 == 0:
            list0[i] = 0 #第3個人標記爲0，在下一個循環不進行計數

    #print '-' * 20
    #print list0

    countend = countend % 3 #看完成1次循環後最後一個人報到幾，應該是0,1,2之一

    breakcount = sum([i > 0 for i in list0])
    if breakcount < 2:
        print "#" * 20
        print "The people left is:"
        for item in list0:
            if item > 0:
                print item
        break
