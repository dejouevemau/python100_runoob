#coding:utf-8
#古典问题：有一对兔子，从出生后第3个月起每个月都生一对兔子，小兔子长到第三个月后每个月又生一对兔子，假如兔子都不死，问每个月的兔子总数为多少对？ 
import time

#解法：实际上就是一个斐波那契数列的一部分，前面ex006.py已经给出了一种解法，这里给出更简单的解法
a = 1
b = 1

n = int(raw_input("Give me the month please:> "))


if n <= 2:
	pass
else:
	for i in range(3, n + 1):
		tempa = b
		b += a
		a = tempa
		
	
time.sleep(0.2)
print "There are %d pairs of rabbits at the %dth month." % (b, n)