#coding:utf-8
#企业发放的奖金根据利润提成。利润(I)低于或等于10万元时，奖金可提10%；利润高于10万元，低于20万元时，低于10万元的部分按10%提成，高于10万元的部分，可提成7.5%；20万到40万之间时，高于20万元的部分，可提成5%；40万到60万之间时高于40万元的部分，可提成3%；60万到100万之间时，高于60万元的部分，可提成1.5%，高于100万元时，超过100万元的部分按1%提成，从键盘输入当月利润I，求应发放奖金总数？

profit_seg = [0, 10, 10, 20, 20, 40, 1000] #最前面添加0很重要，否则利润不足10万的就漏掉了
income_seg = [0, 0.1, 0.075, 0.05, 0.03, 0.015, 0.01] #最前面添加0很重要，否则利润不足10万的就漏掉了
income_seg2 = []



myprofit = int(raw_input("The profit is (in 10,000s):> ")) #注意这个int很容易漏！

for i in range(1, len(profit_seg)): 
#上面的i从1开始是为了配合后面的sum，因为sum中的列表切片操作不含右侧index，如果切出来[0:0]会返回空值
	income_seg2.append(profit_seg[i - 1] * income_seg[i - 1])
	if sum(profit_seg[0:i]) < myprofit <= sum(profit_seg[0:i + 1]):
		myincome = (myprofit - sum(profit_seg[0:i])) * income_seg[i] + sum(income_seg2)
		break
	else:
		pass
		
print "Congratulations! You will get %2f (in 10,000)!" % myincome