#coding:utf-8
#给出斐波那契数列的第n个元素

n = int(raw_input("Which element in Fibonacci array do you want? :> "))

f = [0, 0, 0, 1]

print '-' * 20

if n <= 2:
	print f[n + 1]
else:
	for i in range(3, n + 1):
		f.append(f[i - 1] + f[i])
	print f[-1]