#coding:utf-8
#输入某年某月某日，判断这一天是这一年的第几天？

date = raw_input("Please input date in the format[YYYYMMDD]:> ")
year = int(date[0:4])
month = int(date[4:6])
day = int(date[6:8])

days = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]


#判断闰年
if year % 4 == 0:
	if year % 100 == 0 and year % 400 > 0:
		pass
	elif year % 3200 == 0:
		pass
	else:
		days[2] = 29


which_day = sum(days[0:month]) + day


print '-' * 20
print "This is the %dth day of year %d." %(which_day, year)
print "%d%% of this year has passed." % (100 * which_day / sum(days)) 
#注意%后面如果有运算，需要括起来，不然会出错
#注意%要用%来转义，其他特殊字符用\来转义