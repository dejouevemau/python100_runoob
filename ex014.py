#coding:utf-8
#将一个正整数分解质因数。例如：输入90,打印出90=2*3*3*5
import math


#首先構建一個函數，把x分解爲兩個因數，如果x是素數，則分解爲1 * x本身
def divide2(x):
    upper = int(math.ceil(math.sqrt(x))) + 1
    #假設兩個因數不相等，則較小的那個因數一定小於x的平方根
    #假設兩個因數相等，則較小的那個因數等於x的平方根
    #所以上面的upper，即較小因數的上界，應該+1
    for i in range(2, upper):
        if x % i == 0: 
        #用餘數來判斷是否可以整除比較好，x == int(x)這種方法會出錯 
            return i, (x / i)
            break
        elif i == upper - 1:
        #如果循環至此還沒有找到因數，則說明x是素數，返回1和x本身
            return 1, x
        else:
            pass

x = int(raw_input("Please input a positive integer:> "))

f0 = [x]

while True: #关于while：最好用while True，然后通过一个if...break条件来退出循环
	f1 = [] #每次都初始化f1这个暂时容器，以免出现重复值
	for item in f0:
		a, b = divide2(item)
		if a == 1 or b == 1:
			f1.append(item) #如果该元素不可分解，则返回其本身
		else:
			f1.append(a)
			f1.append(b)
		
	if len(f1) == len(f0): #如果f0经过逐个分解操作后和f1大小一样，说明其中每个元素都无法分解
		#print "Encountering prime number. Exit."
		break
	else:
		f0 = f1

		
fs = []

for item in f0:
    fs.append(str(item))  #把f中的元素都轉換爲字符串

print '-' * 20


if len(fs) > 1:
    print "%d=%s" % (x, '*'.join(fs))
else:
    print "%d is a prime number." % x