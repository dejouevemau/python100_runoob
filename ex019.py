#coding:utf-8
#一个数如果恰好等于它的因子之和，这个数就称为"完数"。例如6=1＋2＋3。编程找出1000以内的所有完数
import math

#定义函数：给定一个正整数，找出它的所有因数
def find_f(x):
	list_f = []
	myrange = range(1, int(math.sqrt(x)) + 1)
	#不管x是不是一个平方数，这个算式都可得出正确结果，因为int()函数是向下取整的
	for i in myrange:
		if x % i == 0:
			list_f.append(i)
			list_f.append(x / i)
	f = list(set(list_f)) #去除重复元素
	f.sort()
	f.pop(-1) #去除x本身
	return f
	

for i in range(1, 1001):
	if sum(find_f(i)) == i:
		print i
		print find_f(i)