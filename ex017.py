#coding:utf-8
#输入一行字符，分别统计出其中英文字母、空格、数字和其它字符的个数
#思路：使用列表推导式定义几个字符串列表

chars = [chr(i) for i in range(97, 123)] #这是获得字母表的方法
Chars = [s.title() for s in chars]

nums = [str(i) for i in range(0, 10)]

mystr = raw_input("Give me a string:> ")

result = [0] * 4
name = ['charactors', 'numbers', 'spaces', 'other elements']

for item in mystr:
	if item in chars or item in Chars:
		result[0] += 1
	elif item in nums:
		result[1] += 1
	elif item == ' ':
		result[2] += 1
	else:
		result[3] += 1


print '-' * 20

for i in range(0, 4):
	print "There are %d %s." % (result[i], name[i])