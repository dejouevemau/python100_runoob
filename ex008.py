#coding:utf-8
#输出 9*9 乘法口诀表

for i in range(1,10):
	for j in range(1,i + 1):
		print "%d*%d=%d\t" % (i, j, i * j), #注意这个逗号和前面格式串中的\t
	print "\n"