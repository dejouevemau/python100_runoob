#coding:utf-8
#求100之内的素数

v = range(1,101)

for i in v:
	for j in range(2, 10):
		if i % j == 0:
			break
	else: #注意这里for跟else的配合。前面的break是打断的第二个for包含的代码段，如果不加这个else，则每一个i都会被打印
		print i