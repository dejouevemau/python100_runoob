#coding:utf-8
#两个乒乓球队进行比赛，各出三人。甲队为a,b,c三人，乙队为x,y,z三人。已抽签决定比赛名单。有人向队员打听比赛的名单。a说他不和x比，c说他不和x,z比，请编程序找出三队赛手的名单



for i in range(3):
	for j in range(2):
		for k in range(3):
			for m in range(2):
				t1 = ['a', 'b', 'c']
				t1r = []
				t1r.append(t1.pop(i))
				t1r.append(t1.pop(j))
				t1r += t1
				t2 = ['x', 'y', 'z']
				t2r = []
				t2r.append(t2.pop(k))
				t2r.append(t2.pop(m))
				t2r += t2
				combined = dict(zip(t1r, t2r))
				if combined['a'] != 'x' and combined['c'] == 'y':
					print "%s--%s %s--%s %s--%s" % (t1r[0], t2r[0], 
					t1r[1], t2r[1], t1r[2], t2r[2])